<?php

class MultiLevelSoftCacheTest extends \PHPUnit\Framework\TestCase
{
    public function testMultiLevelSoftCache()
    {
        $softCache = new \Cyberrebell\SoftTtlCachePhp\MultiLevelSoftCache(
            new \Symfony\Component\Cache\Adapter\ArrayAdapter(1),
            new \Symfony\Component\Cache\Adapter\ArrayAdapter(5),
            3
        );


        //test simple new value
        $item = $softCache->getItem(
            'test',
            function () {
                return 3;
            }
        );
        $this->assertEquals(
            3,
            $item->get()
        );


        //test outdated value when first level ttl is not over
        $item = $softCache->getItem(
            'test',
            function () {
                return 4;
            }
        );
        $this->assertEquals(
            3,  //old value is still cached
            $item->get()
        );


        //request value again when first level ttl is over
        sleep(2);
        $item = $softCache->getItem(
            'test',
            function () {
                return 4;
            }
        );
        $this->assertEquals(
            3,  //first level cache will get the outdated value from master cache again
            $item->get()
        );


        //request value again when master soft ttl is over
        sleep(2);
        $item = $softCache->getItem(
            'test',
            function () {
                return 4;
            }
        );
        $this->assertEquals(
            4,  //master soft ttl is over so the value is refreshed
            $item->get()
        );


        //request value again when first level ttl is over
        sleep(2);
        $item = $softCache->getItem(
            'test',
            function () {
                return 5;
            }
        );
        $this->assertEquals(
            4,  //first level cache will get the outdated value from master cache again
            $item->get()
        );


        //request value again when master hard ttl is over
        sleep(4);
        $item = $softCache->getItem(
            'test',
            function () {
                return 5;
            }
        );
        $this->assertEquals(
            5,  //master hard ttl is over so the value is refreshed
            $item->get()
        );
    }
}