<?php

class SoftCacheTest extends \PHPUnit\Framework\TestCase
{
    public function testSoftCache()
    {
        $softCache = new \Cyberrebell\SoftTtlCachePhp\SoftCache(
            new \Symfony\Component\Cache\Adapter\ArrayAdapter(),
            1
        );


        //test simple new value
        $item = $softCache->getItem(
            'test',
            function () {
                return 3;
            }
        );
        $this->assertEquals(
            3,
            $item->get()
        );


        //test outdated value when ttl is not over
        $item = $softCache->getItem(
            'test',
            function () {
                return 4;
            }
        );
        $this->assertEquals(
            3,
            $item->get()
        );


        //request value again when ttl is over
        sleep(2);
        $item = $softCache->getItem(
            'test',
            function () {
                return 4;
            }
        );
        $this->assertEquals(
            4,
            $item->get()
        );
    }
}