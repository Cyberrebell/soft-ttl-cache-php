<?php

namespace Cyberrebell\SoftTtlCachePhp;

use Psr\Cache\CacheItemInterface;
use Psr\Cache\CacheItemPoolInterface;

/**
 * Works just like SoftCache
 *
 * The additional first level cache will update from master cache if outdated.
 */
class MultiLevelSoftCache extends SoftCache implements SoftCacheInterface
{
    protected $firstLevelCache;


    public function __construct(
        CacheItemPoolInterface $firstLevelCache,
        CacheItemPoolInterface $masterCache,
        int $softTtl = 3600
    )
    {
        $this->firstLevelCache = $firstLevelCache;
        parent::__construct($masterCache, $softTtl);
    }

    /**
     * @inheritDoc
     */
    public function getItem(string $key, callable $refreshValueCallback): CacheItemInterface
    {
        $item = $this->firstLevelCache->getItem($key);
        if (!$item->isHit()) {
            $masterCacheItem = parent::getItem($key, $refreshValueCallback);
            $item->set($masterCacheItem->get());
            $this->firstLevelCache->save($item);
        }

        return $item;
    }

    public function save(CacheItemInterface $item): bool
    {
        return parent::save($item) && $this->firstLevelCache->save($item);
    }

    public function deleteItem(string $key): bool
    {
        return parent::deleteItem($key) && $this->firstLevelCache->deleteItem($key);
    }
}