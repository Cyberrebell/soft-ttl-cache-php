<?php

namespace Cyberrebell\SoftTtlCachePhp;

use Psr\Cache\CacheItemInterface;
use Psr\Cache\CacheItemPoolInterface;

/**
 * Psr cache wrapper that provides soft ttl
 * (when soft ttl expired the value will be refreshed. If refresh failed it will return the old hard cached value)
 */
class SoftCache implements SoftCacheInterface
{
    protected $cache;
    protected $softTtl;


    public function __construct(CacheItemPoolInterface $cache, int $softTtl = 3600)
    {
        $this->cache = $cache;
        $this->softTtl = $softTtl;
    }

    /**
     * @inheritDoc
     */
    public function getItem(string $key, callable $refreshValueCallback): CacheItemInterface
    {
        $item = $this->cache->getItem($key);
        if ($item->isHit()) {
            $value = $item->get();
            if ($value[self::SOFT_EXPIRATION] < time()) {
                $this->updateItem($item, $refreshValueCallback);
            } else {
                $item->set($value[self::CACHE_ITEM]);
            }
        } else {
            $this->updateItem($item, $refreshValueCallback);
        }
        return $item;
    }

    public function save(CacheItemInterface $item): bool
    {
        $oldValue = $item->get();
        $item->set([
            self::SOFT_EXPIRATION => time() + $this->softTtl,
            self::CACHE_ITEM => $oldValue
        ]);
        $success = $this->cache->save($item);
        $item->set($oldValue);  //revert value for further processing
        return $success;
    }

    public function deleteItem(string $key): bool
    {
        return $this->cache->deleteItem($key);
    }

    protected function updateItem(CacheItemInterface $item, callable $refreshValueCallback): void
    {
        try {
            $item->set($refreshValueCallback());
            self::save($item);
        } catch (RefreshFailedException $exception) {}
    }
}