<?php

namespace Cyberrebell\SoftTtlCachePhp;

use Psr\Cache\CacheItemInterface;

interface SoftCacheInterface
{
    const SOFT_EXPIRATION = 's';
    const CACHE_ITEM = 'i';


    /**
     * Get the item from cache. If outdated the $refreshValueCallback will be used to refresh the value.
     *
     * @param string $key
     * @param callable $refreshValueCallback
     * @return CacheItemInterface
     */
    public function getItem(string $key, callable $refreshValueCallback): CacheItemInterface;

    public function save(CacheItemInterface $item): bool;

    public function deleteItem(string $key): bool;
}
