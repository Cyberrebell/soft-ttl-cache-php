# soft-ttl-cache-php

## SoftCache
* Psr cache wrapper that provides soft ttl
* When soft ttl expired the value will be refreshed. If refresh failed it will return the old hard cached value.

## MultiLevelSoftCache
* Works just like SoftCache
* The additional first level cache will update from master cache if outdated.